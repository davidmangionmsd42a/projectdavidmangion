﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSound : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<AudioSource>().Play();
		/*Techincally we do not need this script and code since the sound
		should be played as soon as the laser is created and the settings
		are readily set from the Unity Editor. Reason being, the audio source
		is set to Play On Awake so as soon as the laser is recloned/generated,
		the audio clip will play immediately.
		If we need to play the sound at a particular time and NOT upon
		generation of the object, we need to untick Play on Awake and use
		the above code to play the sound when required.
		*/
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
