﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour {

    [SerializeField] GameObject enemyLaser;
    
    [SerializeField] float laserSpeed = -10f;

    public GameObject particle;

    float laserFiringTime;
    
	void Start () {
		StartCoroutine(FireContinously());
	}
	
	// Update is called once per frame
	void Update () {
	}
    
    
    IEnumerator FireContinously()
    {
        while (true)
        {
            SoundManagerScript.PlaySound("alienLaser");
            GameObject enemyLaserClone = Instantiate(enemyLaser, transform.position, Quaternion.identity) as GameObject;
            enemyLaserClone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, laserSpeed);
            
            laserFiringTime = Random.Range(0.5f, 0.8f);
            yield return new WaitForSeconds(laserFiringTime);
        }
    

    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.tag == "playerlaser"){

            SoundManagerScript.PlaySound("alienDeath");
            Instantiate (particle, transform.position, Quaternion.identity);
            Player.playerScore += 10;
            Destroy(gameObject);
            Destroy(collider.gameObject);

        }
    }
}
