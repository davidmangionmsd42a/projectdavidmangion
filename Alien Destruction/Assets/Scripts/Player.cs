﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float padding = 1f;
    [SerializeField] float projectileSpeed = 20f;
    [SerializeField] float projectileFiringTime = 0.1f;
    [SerializeField] GameObject laserPrefab;
    Coroutine firingCoroutine;

    Text hp;
    Text score;
    int playerHealth = 200;
    public static int playerScore = 0;

    float xMin;
    float xMax;
    float yMin;
    float yMax;
    private int count;

    void Start()
    {
        SetUpMoveBoundries();

        hp = GameObject.Find("HealthText").GetComponent<Text>();

        score = GameObject.Find("ScoreText").GetComponent<Text>();
        hp.text = playerHealth.ToString();
    }

    void Update()
    {
        Move();
        Fire();
        UpdateHP();
        UpdateScore();
        win();
    }

    void Move()
    {

        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);

        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);
    }

    
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "enemylaser")
        {
            playerHealth -= 10;
            Destroy(collider.gameObject);
            
        }
    }

    void SetUpMoveBoundries()
    {
       
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }

    void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            firingCoroutine = StartCoroutine(FireContinously());
        }

        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCoroutine);
        }
    }

    void UpdateHP()
    {
        hp.text = playerHealth.ToString();
        if (playerHealth <= 0)
        {
            playerHealth = 0;
            SceneManager.LoadScene("Lose");
        }
    }

    void UpdateScore()
    {
        score.text = playerScore.ToString();
    }

    void win()
    {
        if (score.text == "500")
        {
            SceneManager.LoadScene("Win");

        }
    }

    IEnumerator PrintAndWait()
    {
        Debug.Log("Message 1 sent");
        yield return new WaitForSeconds(3);
        Debug.Log("Message 2 Sent lmao this was sent after 3 seconds.");

    }

    IEnumerator FireContinously()
    {
        while (true)
        {
            SoundManagerScript.PlaySound("playerLaser");
            GameObject laserClone = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
            laserClone.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);

            yield return new WaitForSeconds(projectileFiringTime);
        }


    }

}
