﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    public void LoadLevel(string name)
    {
        Debug.Log("Loading Level" + name);
        SceneManager.LoadScene(name);
    }

    public void QuitGameEXE()
    {
        Application.Quit();
    }

    public void QuitGameUnity()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }
}